# repo-utils 3.1.1

General utilities for NPM repositories

[![developtment time](https://bytes.nz/b/repo-utils/custom?color=yellow&name=development+time&value=~1+hours)](https://gitlab.com/bytesnz/repo-utils/blob/master/.tickings)
[![contributor covenant](https://bytes.nz/b/repo-utils/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/bytesnz/repo-utils/blob/master/code-of-conduct.md)

# Repository Labels

A [labelify](https://github.com/itgalaxy/labelify) config is included to
configure development labels for Gitlab or Github. Once a package.json file
has been configured for the project, `labelify` can be run from the project
folder to install the labels on the project.

```
npm i -g labelify
TOKEN=gittoken labelify --config repo-utils/labelify.config.js
```

<details>
<summary>labelify.config.json Configuration file</summary>

```js
{
  "labels": [
    {
      "color": "#ffffff",
      "name": "priority:: - 1 highest",
      "priority": 11
    },
    {
      "color": "#ffffff",
      "name": "priority:: - 2 high",
      "priority": 12
    },
    {
      "color": "#ffffff",
      "name": "priority:: - 3 medium",
      "priority": 13
    },
    {
      "color": "#ffffff",
      "name": "priority:: - 4 low",
      "priority": 14
    },
    {
      "color": "#ffffff",
      "name": "priority:: - 5 lowest",
      "priority": 15
    },
    {
      "color": "#ad8d43",
      "name": "bonus::accessibility",
      "description": "Will improve accessibility"
    },
    {
      "color": "#ad8d43",
      "name": "bonus::interoperability",
      "description": "Will improve data sharing and interoperability"
    },
    {
      "color": "#ad8d43",
      "name": "bonus::performance",
      "description": "Will improve performance"
    },
    {
      "color": "#ad8d43",
      "name": "bonus::security",
      "description": "Will improve security"
    },
    {
      "color": "#428bca",
      "name": "size::epic",
      "description": "Issue of epic proportions. Must be broken down into stories."
    },
    {
      "color": "#428bca",
      "name": "size::task",
      "description": "A task. Can be released or part of a story"
    },
    {
      "color": "#428bca",
      "name": "size::story",
      "description": "Story-sized issue. Should be broken down into tasks"
    },
    {
      "color": "#8e44ad",
      "name": "status::ready"
    },
    {
      "color": "#8e44ad",
      "name": "status::developing"
    },
    {
      "color": "#000000",
      "name": "status::blocked"
    },
    {
      "color": "#8e44ad",
      "name": "status::review"
    },
    {
      "color": "#004e00",
      "name": "type::feature"
    },
    {
      "color": "#004e00",
      "name": "type::maintenance"
    },
    {
      "color": "#004e00",
      "name": "type::improvement"
    },
    {
      "color": "#cc0033",
      "name": "type::bug",
      "priority": 1
    }
  ]
}

```
</details>

# `mdFileInclude.cjs`

Replace &lt;!&gt; tags with a file, file part, JSON or package information

- &lt;!=*file*&gt;
- &lt;!=*file* *from*-*to*&gt;
- &lt;!=*file* *from*-&gt;
- &lt;!=*file* -*to*&gt;

  Will be replaced with the contents of file *file*, optionally only the
  lines *from* to *to*

- &lt;!=*json_file.json* *path/to/value*&gt;
- &lt;!=*json_file.json* *path/to/value* *regex*&gt;

  Will be replaced by the value in the JSON file *json_file.json* under
  the path *path/to/value*, optionally using the regular express *regex*
  to extract the first matching capture group

# `compileSchema.mjs`

Convert the JSON schema objects into Typescript interfaces

usage: compileSchema.mjs &lt;directory&gt; &lt;output_directory&gt;

- &lt;directory&gt; is the directory to file the javascript type modules in
- &lt;output_directory&gt; is where the type files will be written to

# `makeJsonSchema.mjs`

Convert the JSON schema objects into JSON files

usage: makeJsonSchema.mjs &lt;directory&gt; &lt;output_directory&gt;

- &lt;directory&gt; is the directory to file the javascript type modules in
- &lt;output_directory&gt; is where the type files will be written to


# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [unreleased]

## [3.2.0] 2022-11-15

- Add `makeJsonSchema.mjs` and `compileSchema.mjs` scripts for converting
  javascript JSON schema modules to JSON files and Typescript interfaces
- Add a `package.json` file that includes dependencies for new scripts

## [3.1.1] 2022-08-08

- Fix to end file include using mdFileInclude

## [3.1.0] 2022-08-03

- Add ability to include a substring from a value using a regular expression.
  The first capture group from the regular expression match will be used.
- Change labilify config to JSON so that it works in ES modules

## [3.0.0] 2021-01-16

- Renamed mdFileInclude to mdFileInclude for node module projects

## [2.0.0] 2021-01-15

- Add ability to specify the lines to be extracted from an included file


## [1.0.0] 2021-01-14

Initial version

[unreleased]: https://gitlab.com/MeldCE/first-draft/compare/v3.1.1...dev
[3.1.1]: https://gitlab.com/MeldCE/first-draft/compare/v3.1.0...v3.1.1
[3.1.0]: https://gitlab.com/MeldCE/first-draft/compare/v3.0.0...v3.1.0
[3.0.0]: https://gitlab.com/MeldCE/first-draft/compare/v2.0.0...v3.0.0
[2.0.0]: https://gitlab.com/MeldCE/first-draft/compare/v1.0.0...v2.0.0
[1.0.0]: https://gitlab.com/bytesnz/repo-utils/tree/v1.0.0

