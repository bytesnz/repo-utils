#!/usr/bin/env node
/**
Replace <!> tags with a file, file part, JSON or package information

- <!=*file*>
- <!=*file* *from*-*to*>
- <!=*file* *from*->
- <!=*file* -*to*>

  Will be replaced with the contents of file *file*, optionally only the
  lines *from* to *to*

- <!=*json_file.json* *path/to/value*>
- <!=*json_file.json* *path/to/value* *regex*>

  Will be replaced by the value in the JSON file *json_file.json* under
  the path *path/to/value*, optionally using the regular express *regex*
  to extract the first matching capture group
*/
const fs = require('fs');
const path = require('path');

const data = {};

if (process.argv.length !== 4) {
  console.error('usage: mkReadme <source> <output>');
  process.exit(1);
}

const getDeep = (object, pathParts) => {
  if (pathParts && pathParts.length) {
    const key = pathParts.shift();

    if (object instanceof Object) {
      return getDeep(object[key], pathParts);
    } else {
      return;
    }
  }

  return object;
}

const source = process.argv[2];
const dir = path.dirname(source);
const out = process.argv[3];

let readme = fs.readFileSync(source).toString();

// Replace <!=<json_file> <path/to/value> [<regex>]>
readme = readme.replace(/<!=(.+?\.json) ([^> ]+)(?: ([^>]+))?>/g, (match, file, key, regex) => {
  if (data[file] === false) {
    return '';
  } else if (!data[file]) {
    const resolved = path.resolve(file);
    try {
      data[file] = require(resolved);
    } catch (error) {
      console.error(`Error reading file '${file} in ${dir}': ${error.message}`);
      data[file] = false;
      return '';
    }
  }

  let value;

  if (key.indexOf('.') !== -1) {
    value = getDeep(data[file], key.split('.')) || '';
  } else if (key.indexOf('/') !== -1) {
    value = getDeep(data[file], key.split('/')) || '';
  } else {
    value = data[file][key] || '';
  }

  if (!regex) {
    return ['string', 'number', 'boolean'].indexOf(typeof value) !== -1
        ? value : JSON.stringify(value, null, 2);
  }

  match = value.match(new RegExp(regex));

  if (!match || !match[1]) {
    return '';
  }

  return match[1];
});

// Replace <!=<file>[ [<from>]-[<to>]]>
readme = readme.replace(/<!=(!?)(.+?)(?: (\d*)-(\d*))?>/g, (match, replace, includePath, from, to) => {
  const resolved = path.resolve(includePath);

  try {
    fs.accessSync(resolved, fs.constants.R_OK);

    let contents = fs.readFileSync(resolved).toString();

    // Replace tabs with spaces
    contents = contents.replace(/\t/gm, '  ');
    if (!replace) {
      contents = contents.replace(/</gm, '&lt;').replace(/>/gm, '&gt;');
    }

    if (from || to) {
      const lines = contents.split(/(?:\n|\r\n)/);

      return lines.slice(from ? from - 1 : 0, to || lines.length).join('\n');
    } else {
      return contents;
    }
  } catch (error) {
    console.log(error);
    console.error(`Error reading file '${includePath}' in '${dir}': ${error.message}`);
    return '';
  }
});

fs.writeFileSync(out, readme);
console.log(`${out} written`);
