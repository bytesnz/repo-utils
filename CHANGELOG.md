# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [unreleased]

## [3.2.0] 2022-11-15

- Add `makeJsonSchema.mjs` and `compileSchema.mjs` scripts for converting
  javascript JSON schema modules to JSON files and Typescript interfaces
- Add a `package.json` file that includes dependencies for new scripts

## [3.1.1] 2022-08-08

- Fix to end file include using mdFileInclude

## [3.1.0] 2022-08-03

- Add ability to include a substring from a value using a regular expression.
  The first capture group from the regular expression match will be used.
- Change labilify config to JSON so that it works in ES modules

## [3.0.0] 2021-01-16

- Renamed mdFileInclude to mdFileInclude for node module projects

## [2.0.0] 2021-01-15

- Add ability to specify the lines to be extracted from an included file


## [1.0.0] 2021-01-14

Initial version

[unreleased]: https://gitlab.com/MeldCE/first-draft/compare/v3.1.1...dev
[3.1.1]: https://gitlab.com/MeldCE/first-draft/compare/v3.1.0...v3.1.1
[3.1.0]: https://gitlab.com/MeldCE/first-draft/compare/v3.0.0...v3.1.0
[3.0.0]: https://gitlab.com/MeldCE/first-draft/compare/v2.0.0...v3.0.0
[2.0.0]: https://gitlab.com/MeldCE/first-draft/compare/v1.0.0...v2.0.0
[1.0.0]: https://gitlab.com/bytesnz/repo-utils/tree/v1.0.0
