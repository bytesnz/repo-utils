# repo-utils 3.1.1

General utilities for NPM repositories

[![developtment time](https://bytes.nz/b/repo-utils/custom?color=yellow&name=development+time&value=~1+hours)](https://gitlab.com/bytesnz/repo-utils/blob/master/.tickings)
[![contributor covenant](https://bytes.nz/b/repo-utils/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/bytesnz/repo-utils/blob/master/code-of-conduct.md)

# Repository Labels

A [labelify](https://github.com/itgalaxy/labelify) config is included to
configure development labels for Gitlab or Github. Once a package.json file
has been configured for the project, `labelify` can be run from the project
folder to install the labels on the project.

```
npm i -g labelify
TOKEN=gittoken labelify --config repo-utils/labelify.config.js
```

<details>
<summary>labelify.config.json Configuration file</summary>

```js
<!=labelify.config.json>
```
</details>

# `mdFileInclude.cjs`

<!=mdFileInclude.cjs 3-18>

# `compileSchema.mjs`

<!=compileSchema.mjs 3-8>

# `makeJsonSchema.mjs`

<!=makeJsonSchema.mjs 3-8>


<!=CHANGELOG.md>
