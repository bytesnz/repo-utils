#!/usr/bin/env node
/**
Convert the JSON schema objects into Typescript interfaces

usage: compileSchema.mjs <directory> <output_directory>

- <directory> is the directory to file the javascript type modules in
- <output_directory> is where the type files will be written to
*/

import fg from 'fast-glob';
import { dirname, resolve, join, basename } from 'path';
//import { register } from 'ts-node';
import { compile } from 'json-schema-to-typescript';
import { argv, cwd, exit } from 'process';
import { statSync, writeFileSync } from 'fs';
import mkdirp from 'mkdirp';
import minimist from './lib/minimist.cjs';

const args = minimist(argv.slice(2));

//register();

const moduleUrl = new URL(import.meta.url);
const _dirname = dirname(moduleUrl.pathname);

const usage = `usage ${basename(argv[1])} [-j dir] [-t dir] <...directory>

  <...directory> Source schema directories

  -j dir Output JSON schema files to dir
  -t dir Output Typescript definition files to dir
`;

function checkOutputDir(outputDir) {
	try {
		const stat = statSync(outputDir);

		if (!stat.isDirectory()) {
			console.error(`Output directory ${output} is not a directory`);
			exit(2);
		}
	} catch (error) {
		if (error.code !== 'ENOENT') {
			console.error(`Error checking output directory: ${error.message}`);
			exit(3);
		}
		mkdirp.sync(outputDir);
	}
}

if (args._.length < 1) {
	console.error(usage);
	exit(1);
}

if (!args.t && !args.j) {
	console.error(
		'Need at least either a JSON output directory or a Typescript ' +
		'definition output directory'
	);
	console.error(usage);
	exit(2);
}

const srcs = args._;

if (args.t) {
	var typescriptDir = resolve(cwd(), args.t);
	checkOutputDir(typescriptDir);
}

if (args.j) {
	var jsonDir = resolve(cwd(), args.j);
	checkOutputDir(jsonDir);
}

for (let i = 0; i < srcs.length; i++) {
	const src = resolve(cwd(), srcs[i]);

	console.log(`Scanning ${src}`);
	fg('**/*.(js|mjs|cjs)', {cwd: src}).then((files) => {
		for (let f = 0; f < files.length; f++) {
			const name = basename(files[f]);
			import(join(src, files[f])).then(async (it) => {
				if (typescriptDir) {
					const output = files[f].slice(0, files[f].lastIndexOf('.')) + '.d.ts';
					await compile(it.default, name, {
						cwd: src,
						bannerComment: `/* tslint:disable */
	/* This file was automatically generated from ${join(srcs[i], files[f])}.
	 * DO NOT MODIFY IT BY HAND. Instead, modify the source${
		 args.c ? ` and run\n * $ ${args.c}` : '.'}
	 */
	`,
						unreachableDefinitions: true
					}).then((def) => {
						writeFileSync(
							join(typescriptDir, output),
							args.n ? `declare namespace ${args.n} {\n${def}\n}` : def
						);
						console.log(`Done ${files[f]} ${output}`);
					});
				}
				if (jsonDir) {
					const output = files[f].slice(0, files[f].lastIndexOf('.')) + '.json';
					writeFileSync(
						join(jsonDir, output),
						JSON.stringify(it.default, null, 2)
					);
					console.log(`Done ${files[f]} ${output}`);
				}
			}).catch((error) => {
				console.error('Failed to compile schema', files[f], error.message);
			});
		}
	});
}
