#!/usr/bin/env node
/**
Convert the JSON schema objects into JSON files

usage: makeJsonSchema.mjs <directory> <output_directory>

- <directory> is the directory to file the javascript type modules in
- <output_directory> is where the type files will be written to
*/


import fg from 'fast-glob';
import { dirname, resolve, join, basename } from 'path';
import { argv, cwd, exit } from 'process';
import { statSync, writeFileSync } from 'fs';
import mkdirp from 'mkdirp';
import minimist from './lib/minimist.cjs';

const args = minimist(argv.slice(2));

//register();

const moduleUrl = new URL(import.meta.url);
const _dirname = dirname(moduleUrl.pathname);

if (args._.length != 2) {
	console.error(`usage: ${basename(argv[1])} <directory> <output_directory>`);
	exit(1);
}

const output = args._.pop();
const srcs = args._;

const outputDir = resolve(cwd(), output);
try {
	const stat = statSync(outputDir);

	if (!stat.isDirectory()) {
		console.error(`Output directory ${output} is not a directory`);
		exit(2);
	}
} catch (error) {
	if (error.code !== 'ENOENT') {
		console.error(`Error checking output directory: ${error.message}`);
		exit(3);
	}
	mkdirp.sync(outputDir);
}

for (let i = 0; i < srcs.length; i++) {
	const src = resolve(cwd(), srcs[i]);

	console.log(`Scanning ${src}`);
	fg('**/*.(js|mjs|cjs)', {cwd: src}).then((files) => {
		for (let f = 0; f < files.length; f++) {
			const output = files[f].slice(0, files[f].lastIndexOf('.')) + '.json';
			const name = basename(files[f]);
			import(join(src, files[f])).then((it) => {
				console.log(`Done ${files[f]}`);
				writeFileSync(
					join(outputDir, output),
					JSON.stringify(it.default, null, 2)
				);
			});
		}
	});
}

